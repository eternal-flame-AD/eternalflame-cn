package main

import (
	"bytes"
	"encoding/base64"
	"errors"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/skip2/go-qrcode"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	text, ok := request.QueryStringParameters["text"]
	if !ok {
		return nil, errors.New("text not specified")
	}
	levelStr, ok := request.QueryStringParameters["level"]
	if !ok {
		levelStr = "medium"
	}
	var level qrcode.RecoveryLevel
	switch levelStr {
	case "low":
		level = qrcode.Low
	case "medium":
		level = qrcode.Medium
	case "high":
		level = qrcode.High
	case "highest":
		level = qrcode.Highest
	default:
		return nil, errors.New("error level not defined")
	}
	minWidth := -1
	if minWidthStr, ok := request.QueryStringParameters["minwidth"]; ok {
		minWidth, _ = strconv.Atoi(minWidthStr)
	}
	pic, err := qrcode.New(text, level)
	if err != nil {
		return nil, err
	}
	codePNG, err := pic.PNG(minWidth)
	if err != nil {
		return nil, err
	}
	b64w := bytes.NewBuffer([]byte{})
	enc := base64.NewEncoder(base64.StdEncoding, b64w)
	if _, err := enc.Write(codePNG); err != nil {
		return nil, err
	}
	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Headers: map[string]string{
			"Content-Type": "image/png",
		},
		Body:            b64w.String(),
		IsBase64Encoded: true,
	}, nil
}

func main() {
	// Make the handler available for Remote Procedure Call by AWS Lambda
	lambda.Start(handler)
}
