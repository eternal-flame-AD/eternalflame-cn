---
title: 【分享】Travis CI的Pride选项
author: Eternal-flame-AD
layout: post
identifier: 046ce1b6c23fd4ac5d2d1c112536a3ee
date: 2018-04-08T01:46:29+00:00
categories:
  - LGBTQ

---
![](/images/TIM截图20180408094152-1920x941.jpg)

今天偶然发现travis CI调皮了，有一个Pride选项，开启之后导航栏会变成彩虹旗~logo也有了彩虹旗的元素~

> We deeply care about diversity, and strive to be an inclusive organisation. Show your pride (or your solidarity).