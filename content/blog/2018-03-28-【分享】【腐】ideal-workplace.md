---
title: 【分享】【腐】Ideal workplace
author: Eternal-flame-AD
layout: post
identifier: 65064df7c2e9c548a8e5502f32515dc3
date: 2018-03-27T16:48:02+00:00
categories:
  - 日常

---
2014年的post，AT&T能够如此亲和地接纳LGBT人士，可以说非常暖心~

不论文章在编排时是否有夸大和广告的嫌疑，一家以商业利益为主要目的的公司能够将此文章post在官网上，本身就不容置疑地表明了自身企业文化中的Equality

原文链接：[http://about.att.com/content/csr/home/blog/2014/11/my\_experience\_workin.html][1]

摘要：

_As an openly gay general manager at AT&T, I experience AT&T’s commitment to equality and inclusion firsthand; my story as an employee is special because it is unremarkable. From the day I joined AT&T 13 years ago, AT&T fostered an environment where I feel comfortable to share who I am. My husband Rob is a part of my work life just like any spouse, and when he and I married, I was proud to share the news with my AT&T family._

_In 1975, AT&T was one of the first Fortune 500 companies to adopt a non-discrimination policy toward sexual orientation and, in 1998, we were one of the first to adopt domestic partner benefits for LGBT employees._

_That’s why I am personally so grateful that for the last 10 years, AT&T received a 100 percent score on the Human Rights Campaign Foundation’s Corporate Equality Index, which measures commitment to equality, non-discrimination and inclusion in the workplace. Today, we at AT&T are excited to share that we received a 100 percent score for the 11th year in a row!_

_AT&T’s commitment to LGBT equality and inclusion is long standing, both inside and outside the company. In 1975, AT&T was one of the first Fortune 500 companies to adopt a non-discrimination policy toward sexual orientation and, in 1998, we were one of the first to adopt domestic partner benefits for LGBT employees. We also established LEAGUE at AT&T in 1987, which is the oldest LGBT Employee Resource Group (ERG) in the nation. In our communities, we stand up for policies that promote equality and inclusion. AT&T is a member of the HRC Business Coalition for Workplace Fairness, working to support the passage of the Employment Non-Discrimination Act (ENDA), a federal bill to protect employees in the workplace._

_As we celebrate this recognition from HRC and the continued progress we are all making to support equality and inclusion, I thank AT&T for its commitment to and continued support of the LGBT community. We have made much progress and there is still work to be done. I look forward to working with my colleagues at AT&T, the Human Rights Campaign and others to continue to strengthen the LGBT community._
  
_About the Author_
  
_Scott Sapperstein_
  
_Executive Director, AT&T Public Affairs_

 [1]: http://about.att.com/content/csr/home/blog/2014/11/my_experience_workin.html