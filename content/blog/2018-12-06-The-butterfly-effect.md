---
title: The Butterfly Effect
date: 2018-12-06T18:19:18+08:00
author: eternal-flame-AD
layout: post
identifier: f60b7aa59d562b5e49f30142d3a47a2f
tags:
  - 电影
categories:
  - 随想
signature: "When the present determines the future, but the approximate present does not approximately determine the future."
---

很致郁的一部电影。。。小时候看过不过当时没太看懂，现在拿出来再看，感触很多。电影的情节联系很紧密，一切似乎都有逻辑的支撑而整个故事确实出乎意料。二合三其实也看了不过觉得多多少少有些套路，还是一最经典～

# Chaotic World

> It has been said that something as small as the flutter of a butterfly's wing can untimately cause a typhoon halfway around the world.
> 
> <cite>——Chaos Theory</cite>

“蝴蝶效应”的名字起源于[混沌原理(Chaos theory)](https://en.wikipedia.org/wiki/Chaos_theory)，一个混沌系统被认为是充满着大量复杂不可计算的关系的混乱但自治的系统。混沌学观点认为，虽然整个世界充满着看似十分逻辑的因果关系，然而很多事情却并不像人们心中所想的那样发展——因为，当每一点点微不足道的变数组合在一起以后，整个系统的未来就会发生翻天覆地的变化。蝴蝶微不足道的拍一下翅膀的能量，有可能就可以被整个世界里各种复杂的关系所传递、转换放大、最终掀起一股足以破坏一切的龙卷风。

Kevin最让人心疼的一句话：<i>Every time I wanted things to get better they just get fucked up.</i>也是这部影片一大致郁点。以为自己拥有“回到过去，改变未来”的能力，没想到最终改变的只是过得最悲惨的人是谁而已。

# Everything Comes with a Price

> Chaos: When the present determines the future, but the approximate present does not approximately determine the future. 
> 
> <cite>——[Edward Lorenz](https://en.wikipedia.org/wiki/Edward_Lorenz)</cite>

Kevin在信箱炸弹一幕里最后选择了冲上前去阻止母女接近信箱，这样的“英雄行为”最终却直接导致了自己的残疾，间接导致了心爱的人离他而去以及母亲患上肺癌。如果说当时Kevin就知道救下2个人也会同时让另外两个人的生活陷入困境，那么Kevin原先的举动还算是“英雄”嘛？或者说，一切只是一个tradeoff而已……学生为了上更好的学校牺牲了自己的兴趣，商人为了更大的利益而承担更大的风险，社会为了保护“大多数人的利益”而排挤“少数人”，如果从结果来看，似乎事情没有什么好坏之分了，所有事情都是牺牲一些东西换来另一些东西。

我们的世界就是这样的吧：多少人喊着“未来的你一定会感谢现在奋斗的你”等等空虚的口号却不知道自己的“奋斗”给自己带来的会是什么。当然小透明也不是说大家都不要努力了，只是觉得努力就是纯粹的想努力——说句玩笑话就是“学习使我快乐”，当这个世界上没有事情是绝对的对错的时候，自由的做自己想做的事情吧～当然**为了自己的自由而侵害别人的自由**是不可取的。

# Mystery

其实有些地方也没太看懂：

- 在Kevin像医生要那些日记而医生说根本没有日记的时候，到底日记存不存在？如果不存在，真的如医生所说那些日记只是他想象出来的话，那整个电影从最早Kevin母亲的两个死胎到最后Kevin选择放弃来到这个世界上都只是Kevin想象出来的事情？真正的Kevin是怎么样的？如果存在，为什么医生要说谎？是医生的谎言让Kevin最终失去活下去的希望嘛？

- 最后Kevin选择不出生算是他歇斯底里的最后的一次“fix everything”的尝试嘛？感觉结果还不错，难受的只有自己和自己的家庭……

# Phonograph

最后放上一首Soundtrack……

{{< ncm "1764080" >}}