---
title: 【AP CSA学习心得】（2）抽象和继承
author: Eternal-flame-AD
layout: post
identifier: 5cc62bcdfc62186d24cca26ca5161b2c
date: 2018-03-27T03:24:02+00:00
categories:
  - Java
  - 学习
tags:
  - AP
  - Computer Science A
  - Python

---
抽象和继承一直是AP CSA考察的重点，在这里先码一段介绍整理一下（知识点只涉及了AP涉及到的部分，求大佬轻喷）

&nbsp;

在定义类时，我们可以加入abstract关键字来定义一个抽象类：

{{< highlight java >}}
abstract class book {
    public static String material="paper";
    public String ISBN;
    public String title;
    public int pagenow;
    protected book() { //constructor dont have return types
        this.pagenow=1; 
    }
    public void flip_page() {
        pagenow++;
        System.out.println("Flipped to page "+pagenow);
    }
}
{{< / highlight >}}

抽象类和class很像，只是不能实例化，因此这个book现在只能读取book.material=&#8221;paper&#8221;
  
抽象类可以被继承成更加具体的类：

{{< highlight java >}}
class AP_Princeton extends book {
    public String ISBN="978-1-101-91988-0";
    public String title="Cracking the AP CSA Exam";
    private static String Customer_review;
    public void flip_page() {
        System.out.println("FLipping page of:"+this.title);
        super.flip_page();  //Not this.super
    }
    public void write_review(String rev) {
        this.Customer_review=rev;
    }
    public String read_review() {
        return Customer_review;
    }
}
{{< / highlight >}}

当然也可以被另一个抽象类继承：

{{< highlight java >}}
abstract class Electronicbook extends book {
    public static String material="Electrical device";
}
class Python_docs extends Electronicbook {
    public String title="Python docs";
}
{{< / highlight >}}

一直需要注意的是，java是一种强类型的语言：如下的代码段会返回paper(不论material是否是static)：

{{< highlight java >}}
book myPythondoc=new Python_docs();
return myPythondoc.material;
{{< / highlight >}}

正确的应该显式转换：

{{< highlight java >}}
book myPythondoc=new Python_docs();
return ((Electronicbook)myPythondoc).material;
{{< / highlight >}}

AP\_Princeton对象可以被存在book类型引用中，但book的其它子类（Electronicbook等）不能被存储在AP\_Princeton类型引用中：

{{< highlight java >}}
book myAPbook=new AP_Princeton(); //This is correct
AP_Princeton myAPbook=new AP_Princeton(); // This is even more correct
Python_docs myAPbook=new AP_Princeton(); //WILL NOT COMPILE
{{< / highlight >}}

最后贴上一段完整的Demo:

{{< highlight java >}}
package classes;
abstract class book {
    public static String material="paper";
    public String ISBN;
    public String title;
    public int pagenow;
    protected book() {
        this.pagenow=1; 
    }
    public void flip_page() {
        pagenow++;
        System.out.println("Flipped to page "+pagenow);
    }
}
abstract class Electronicbook extends book {
    public static String material="Electrical device";
}
class AP_Princeton extends book {
    public String ISBN="978-1-101-91988-0";
    public String title="Cracking the AP CSA Exam";
    private static String Customer_review;
    public void flip_page() {
        System.out.println("FLipping page of:"+this.title);
        super.flip_page();  //Not this.super
    }
    public void write_review(String rev) {
        this.Customer_review=rev;
    }
    public String read_review() {
        return Customer_review;
    }
}
class Python_docs extends Electronicbook {
    public String title="Python docs";
}
public class AbstractDemo {
    public static void main(String[] args) {
        //You can NOT instantiate a book object
        System.out.println(book.material); //paper
        book myAPbook=new AP_Princeton();
        myAPbook.flip_page();
        /*
         * FLipping page of:Cracking the AP CSA Exam
         * Flipped to page 2
         */
        ((AP_Princeton)myAPbook).write_review("Sucks..."); //explicitly tell java this is an instance of AP_Princeton or it will NOT compile
        AP_Princeton anotherAPbook=new AP_Princeton();
        System.out.println(anotherAPbook.read_review()); //Sucks...
        book myPythondoc=new Python_docs();
        System.out.println(myPythondoc.material); // paper !!!!
        System.out.println(((Electronicbook)myPythondoc).material); //Electrical device
    }
}
{{< / highlight >}}