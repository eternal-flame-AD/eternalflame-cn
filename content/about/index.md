---
title: About
date: 2018-12-05T20:57:06+08:00
identifier: 603c1da6359500e1fd473238c75e2402
signature: 小透明的资料
description: 小透明的资料
---

## Coding

要说小透明最喜欢投入的事情大概就是Coding了吧，自己一个人分析问题，解决问题，讨论问题……很棒的体验。主要项目都在GitHub上，个人的一些项目（比如[这个网站](https://gitlab.com/eternal-flame-AD/eternalflame-cn)）会在GitLab上。关注一下～

{{< gitlab eternal-flame-AD >}}
{{< github eternal-flame-AD >}}

{{< github-calendar eternal-flame-AD >}}

## Running

长跑是坚持很久的运动了，也教会了小透明很多[运动之外的东西](/blog/2018/09/%E4%BB%8A%E5%A4%A9%E4%B8%8D%E6%83%B3%E8%B7%91%E6%89%80%E4%BB%A5%E6%89%8D%E5%8E%BB%E8%B7%91/)。不管以后会走什么样的道路，相信长跑会是一直坚持的事情～

{{< racetable >}}

{{< strava 36585979 17f7f3ad07ecc6260d7c0cc7920b890a48678700 >}}

## Life

关注一下小透明的[Twitter](https://twitter.com/eternalflame_AD)吧～ʕ•̀ω•́ʔ✧

## Anime

本命是[兼定定](https://zh.moegirl.org/%E5%88%80%E5%89%91%E4%B9%B1%E8%88%9E:%E5%92%8C%E6%B3%89%E5%AE%88%E5%85%BC%E5%AE%9A#)(*´∀｀)！！！

## Privacy

我的[Keybase](https://keybase.io/eternal_flame)，可以去[这里](https://keybase.io/eternal_flame/pgp_keys.asc)下载我的PGP Public Key~

## Contact

`echo "ef#eternalflame.cn" | sed "s/#/@/"`