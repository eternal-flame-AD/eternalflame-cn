(function(){
    'use strict';

    function insertAfter(newNode, referenceNode) {
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function removeHash(url) {
        let hashPos = url.lastIndexOf("#")
        if (hashPos!==-1) {
            return url.substr(0, hashPos)
        }
        return url
    }

    let article = document.querySelector("article")
    if (article) {
        for (let tagName of ["h1","h2"]) {
            article.querySelectorAll(tagName).forEach(elem=>{
                let icon = document.createElement("i")
                icon.className = "fas fa-link hash-link"
                let link = document.createElement("a")
                link.href = removeHash(document.URL) + `#${elem.id}`
                link.appendChild(icon)
                insertAfter(link,elem)
            })
        }
    }
})()