{
    // common libs
    (function() {
        let keys = [];
        window.enqueue_script = function(key, url, options) {
            if (key) {
                if (keys.includes(key)) {
                    return Promise.resolve()
                }
                keys.push(key)
            }
            let is_async = options?!!options.async:false
            let integrity = options?options.integrity:undefined
            let elt =document.createElement('script');
            elt.charset='utf-8';
            elt.async = is_async;
            elt.crossOrigin = "anonymous";
            if (integrity) elt.integrity = integrity
            return new Promise((resolve, reject)=>{
                elt.addEventListener('load',resolve);
                elt.addEventListener('error',reject);
                elt.src = url
                document.documentElement.appendChild(elt);
            })
        }
    })();
    (function() {
        let keys = [];
        window.enqueue_stylesheet = function(key, url, options) {
            if (key) {
                if (keys.includes(key)) {
                    return Promise.resolve()
                }
                keys.push(key)
            }
            let integrity = options?options.integrity:undefined
            let elt =document.createElement('link')
            elt.rel = 'stylesheet'
            elt.crossOrigin = "anonymous"
            if (integrity) elt.integrity = integrity
            return new Promise((resolve, reject)=>{
                elt.href = url
                document.documentElement.appendChild(elt);
                resolve()
            })
        }
    })();
}

{
    // qr-link
    $(function(){
        if (window.screen.width<720) return;
        document.querySelectorAll("a.link-qr").forEach(el=>{
            $(el).popover({
                html: true,
                trigger: "hover",
                placement: "top",
                content: `<img src="/.netlify/functions/qr?text=${encodeURIComponent(el.href)}&level=medium&minwidth=150" />`,
            })
        })
    })
}